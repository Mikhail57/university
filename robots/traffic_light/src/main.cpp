#include <Arduino.h>

const int autoRedPin = 9;
const int autoGreenPin = 10;

const int peopleRedPin = 11;
const int peopleGreenPin = 12;

const int photoPin = A0;

void turnRG(int redPin, int greenPin, byte red, byte green) {
  analogWrite(redPin, 255 - red);
  analogWrite(greenPin, 255 - green);
}

void turnAutoRG(byte red, byte green) {
  turnRG(autoRedPin, autoGreenPin, red, green);
}

void turnFuckingRG(byte red, byte green) {
  turnRG(peopleRedPin, peopleGreenPin, red, green);
}

void turnAutoGreen() {
  turnAutoRG(0, 255);
}

void turnAutoYellow() {
  turnAutoRG(100, 150);
}

void turnAutoRed() {
  turnAutoRG(255, 0);
}

void turnAutoOff() {
  turnAutoRG(0, 0);
}

void turnBlinkingAutoGreen() {
  for (int i = 0; i < 3; i++) {
    turnAutoGreen();
    delay(500);
    turnAutoOff();
    delay(500);
  }
}

void turnBlinkingPeopleGreen() {
  for (int i = 0; i < 3; i++) {
    turnFuckingRG(0, 255);
    delay(500);
    turnFuckingRG(0, 0);
    delay(500);
  }
}

int getPhotoValue() {
  return analogRead(photoPin);
}

void handleCommand(char command) {
  switch (command) {
    case 'r': turnFuckingRG(255, 0); break;
    case 'g': turnFuckingRG(0, 255); break;
    case 'b': turnBlinkingPeopleGreen(); break;
    case 'R': turnAutoRed(); break;
    case 'G': turnAutoGreen(); break;
    case 'Y': turnAutoYellow(); break;
    case 'B': turnBlinkingAutoGreen(); break;
    case 'P': Serial.println(getPhotoValue());
  }
}

void setup() {
  Serial.begin(9600);

  pinMode(autoRedPin, OUTPUT);
  pinMode(autoGreenPin, OUTPUT);
  pinMode(peopleRedPin, OUTPUT);
  pinMode(peopleGreenPin, OUTPUT);

  pinMode(photoPin, INPUT);
}

void loop() {
  if (!Serial.available())
    return;
  char command = Serial.read();
  handleCommand(command);
}