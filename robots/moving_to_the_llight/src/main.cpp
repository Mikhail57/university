#include <Arduino.h>

#define LEFT_SENSOR A0
#define RIGHT_SENSOR A1

void setup() {
    Serial.begin(9600);
}

short getLeftSensorData() {
    return analogRead(LEFT_SENSOR);
}

short getRightSensorData() {
    return analogRead(RIGHT_SENSOR);
}

void executeCommand(char command) {
    switch (command) {
        case 'c': 
            Serial.print(getLeftSensorData());
            Serial.print(" ");
            Serial.println(getRightSensorData());
            break;
    }
}

void loop() {
    if (Serial.available()) {
        char ch = Serial.read();
        Serial.print("#Command: ");
        Serial.println(ch);
        executeCommand(ch);
    }
}