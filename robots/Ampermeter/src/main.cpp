#include <Arduino.h>

const int pin1 = A0;
const int pin2 = A1;

void setup() {
  Serial.begin(9600);
}

void loop() {
  int dU = (analogRead(pin1) - analogRead(pin2)) * 5. / 1023;
  float I = dU / 98.6;
  Serial.print("I: ");
  Serial.println(I);
}