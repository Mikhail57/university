#include <Arduino.h>

typedef uint8_t byte;

const byte pin = A0;
const int r0 = 9890;
const long resistances[]{216, 978, 99000};
const byte currentResitorPosition = 1;

void setup() {
  pinMode(pin, INPUT);
  Serial.begin(9600);
}

void loop() {
  float value = analogRead(pin);
  float dU = value * 5 / 1023;
  const long currentResitance = resistances[currentResitorPosition];
  float resistons = (currentResitance * (5 - dU)) / dU;
  Serial.print("O: ");
  Serial.println(resistons);
  delay(250);
}

