#include <Arduino.h>
#include <TroykaIMU.h>

typedef uint8_t byte;

const byte LED_PIN = 13;


const int SIZE = 20;
float **values = new float*[SIZE];
int valuesPos = 0;
boolean shakingPrev = false;
boolean ledOn = false;

float prevAX = 0, prevAY = 0, prevAZ = 0;

// создаём объект для работы с акселерометром
Accelerometer accel;

 
void setup() {
  // открываем последовательный порт
  Serial.begin(9600);
  // выводим сообщение о начале инициализации
  Serial.println("Begin init...");
  // инициализация акселерометра
  accel.begin();
  // выводим сообщение об удачной инициализации
  Serial.println("Initialization completed");
  pinMode(LED_PIN, OUTPUT);
  for (byte i = 0; i < 3; i++) {
    values[i] = new float[3];
  }
}

boolean isShaking(float **values, int size) {
  int shaked = 0;
  float low[3]{values[0][0], values[0][1], values[0][2]};
  float high[3]{values[0][0], values[0][1], values[0][2]};
  for (byte i = 1; i < size; i++) {
    for (byte j = 0; j < 3; j++) {
      low[j] = min(values[i][j], low[j]);
      high[j] = max(values[i][j], low[j]);
    }
  }
  float diffSign = 0;
  for (byte i = 0; i < 3; i++) {
    if (low[i] < 0 && high > 0)
      diffSign++;
  }
  return diffSign > 0;
}

void loop() {
  float ax = accel.readAX();
  float ay = accel.readAY();
  float az = accel.readAZ();
  float module = sqrt(ax*ax + ay*ay + az*az);
  float dx = ax - prevAX;
  float dy = ay - prevAY;
  float dz = az - prevAZ;
  // float module = abs(ax + ay + az - prevAX - prevAY - prevAZ);
  values[valuesPos][0] = dx;
  values[valuesPos][1] = dy;
  values[valuesPos][2] = dz;
  valuesPos++;
  if (valuesPos <= SIZE) {
    delay(600 / SIZE);
    return;
  } else {
    valuesPos = 0;
  }
  boolean shaking = isShaking(values, SIZE);
  if (shaking && !shakingPrev) {
    ledOn = !ledOn;
    digitalWrite(LED_PIN, ledOn);
  }
  shakingPrev = shaking;

  // вывод направления и величины ускорения в м/с² по оси X
  Serial.print(ax);
  Serial.print("\t");
  // вывод направления и величины ускорения в м/с² по оси Y
  Serial.print(ay);
  Serial.print("\t");
  // вывод направления и величины ускорения в м/с² по оси Z
  Serial.print(az);
  Serial.print("\t");
  Serial.print(module);
  Serial.println("");

  // delay(200);
}