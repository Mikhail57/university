#include <Arduino.h>
#include <NewPing.h>

// Motors
#define SPEED_LEFT 5
#define SPEED_RIGHT 6
#define DIR_LEFT 4
#define DIR_RIGHT 7

// Sonars
#define SONAR_NUM 2      // Number of sensors.
#define MAX_DISTANCE 300 // Maximum distance (in cm) to ping.

#define THRESHOLD_MIN 10
#define THRESHOLD_MAX 20

// Sensor object array.
// Each sensor's trigger pin, echo pin, and max distance to ping. 
NewPing sonar[SONAR_NUM] = {
  NewPing(8, 9, MAX_DISTANCE), // front
  NewPing(10, 11, MAX_DISTANCE), // left
};

/**
 * Movements section
**/

void go(bool dir_l, bool dir_r, unsigned short speed_l, unsigned short speed_r) {
    digitalWrite(DIR_LEFT, dir_l);
    digitalWrite(DIR_RIGHT, dir_r);
    analogWrite(SPEED_RIGHT, speed_r * .9);
    analogWrite(SPEED_LEFT, speed_l);
}

void forward(unsigned char velocity) {
    go(true, true, velocity, velocity);
}

void backward(unsigned char velocity) {
    go(false, false, velocity, velocity);
}

void turnRight(unsigned char velocity) {
    go(false, true, velocity, velocity);
}

void turnLeft(unsigned char velocity) {
    go(true, false, velocity, velocity);
}

void turnSoftLeft(unsigned char velocity) {
    go(true, true, velocity, velocity /2);
}

void turnSoftRight(unsigned char velocity) {
    go(true, true, velocity /2, velocity);
}

/**
 * Sensors section
**/

inline int getRightSonar() {
    int val = sonar[1].ping_cm();
    return (val==0) ? 100000 : val;
}

inline int getFrontSonar() {
    int val = sonar[0].ping_cm();
    return (val==0) ? 100000 : val;
}


int default_speed = 200;


void setupDefaultSpeed() {
    const int MEASUREMENTS = 3;
    int mils[MEASUREMENTS] = {};
    int dst[MEASUREMENTS] = {};
    forward(200);
    for (char i = 0; i < MEASUREMENTS; i++) {
        mils[i] = millis();
        dst[i] = getFrontSonar();
        delay(300);
    }
    float spd[MEASUREMENTS - 1] = {};
    for (char i = 1; i < MEASUREMENTS; i++) {
        int td = dst[i - 1] - dst[i];
        int tm = mils[i] - mils[i - 1];
        spd[i - 1] = (float)td / tm;
    }
    float avgSpd = 0;
    for (char i = 0; i < MEASUREMENTS - 1; i++) {
        avgSpd += spd[i];
    }
    if (avgSpd > 0.03) {
        default_speed = 255 - 255 * (0.2 - avgSpd) * 5;
    } else {
        default_speed = 250;
    }
    Serial.print("Default speed: ");
    Serial.println(default_speed);
}

void setup() {
    Serial.begin (9600);
    for (short i = 4; i <=7; i++) {
        pinMode(i, OUTPUT);
    }
    setupDefaultSpeed();
}
    
void loop() {
    int sf = getFrontSonar();
    int sr = getRightSonar();

    if (sf > THRESHOLD_MAX) {
        if (sr > THRESHOLD_MAX) {
            forward(default_speed);
            // while (sr > THRESHOLD_MAX) {
            //     sr = getRightSonar();
            //     delay(1);
            // }
        } else if (sr > THRESHOLD_MIN) {
            forward(default_speed);
        } else {
            turnSoftLeft(default_speed);
        }
    } else if (sf > THRESHOLD_MIN) {
        if (sr > THRESHOLD_MAX) {
            turnLeft(default_speed);
        } else if (sr > THRESHOLD_MIN) {
            turnLeft(default_speed);
        } else {
            turnLeft(default_speed);
            // while (sf <= THRESHOLD_MAX) {
            //     sf = getFrontSonar();
            //     delay(1);
            // }
        }
    } else {
        turnLeft(default_speed);
        while (sf < THRESHOLD_MIN) {
            sf = getFrontSonar();
            delay(1);
        }
    }
}