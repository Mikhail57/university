#include <Arduino.h>

#define SPEED_LEFT 5
#define SPEED_RIGHT 6
#define DIR_LEFT 4
#define DIR_RIGHT 7

#define LEFT_SENSOR A0
#define RIGHT_SENSOR A1

#define LEFT_THRESHOLD 150
#define RIGHT_THRESHOLD 150


void setup() {
    for (short i = 4; i <=7; i++) {
        pinMode(i, OUTPUT);
    }
    pinMode(LEFT_SENSOR, INPUT);
    pinMode(RIGHT_SENSOR, INPUT);
    Serial.begin(9600);
}

void go(bool dir_l, bool dir_r, unsigned char speed_l, unsigned char speed_r) {
    digitalWrite(DIR_LEFT, !dir_l);
    digitalWrite(DIR_RIGHT, !dir_r);
    analogWrite(SPEED_RIGHT, speed_r);
    analogWrite(SPEED_LEFT, speed_l);
}

void forward(unsigned char velocity) {
    go(true, true, velocity, velocity);
}

void backward(unsigned char velocity) {
    go(false, false, velocity, velocity);
}

void turnRight(unsigned char velocity) {
    go(false, true, velocity, velocity);
}

void turnLeft(unsigned char velocity) {
    go(true, false, velocity, velocity);
}

short getLeft() {
    return analogRead(LEFT_SENSOR);
}

short getRight() {
    return analogRead(RIGHT_SENSOR);
}

bool isBlackLeft() {
    return getLeft() < LEFT_THRESHOLD;
}

bool isBlackRight() {
    return getRight() < RIGHT_THRESHOLD;
}

const unsigned char speed = 100;

void loop() {
    if (isBlackRight()) {
        if (isBlackLeft()) {
            forward(speed);
        } else {
            turnRight(speed);
            delay(10);
        }
    } else if (isBlackLeft()) {
        turnLeft(speed);
        delay(10);
    } else {
        forward(speed);
    }
}