#include <Arduino.h>

typedef uint8_t byte;

const byte pin = 10;
const byte value = 26;
int onTime = 0;
int offTime = 0;

void calcTime(byte value) {
  const int base = 1000;
  onTime = value * base / 255;
  offTime = base - onTime;
}

void setup() {
  pinMode(pin, OUTPUT);
  calcTime(value);
}

void loop() {
  digitalWrite(pin, HIGH);
  delayMicroseconds(onTime);
  digitalWrite(pin, LOW);
  delayMicroseconds(offTime);
}