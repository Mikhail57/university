#include <Arduino.h>
#include "morze.h"

typedef uint8_t byte;

enum Token {
  UNKNOWN,
  DOT,
  DASH,
  PAUSE,
  LETTER_PAUSE,
};

const byte PHOTO_PIN = A0;

boolean isPhotoLight() {
  return analogRead(PHOTO_PIN) < 200;
}

unsigned waitForLight() {
  unsigned long startTime = millis();
  while(!isPhotoLight()) {
    delayMicroseconds(100);
  }
  // Serial.println("#");
  return millis() - startTime;
}

Token getNextToken() {
  unsigned offTime = waitForLight();
  if (offTime > pause * .8 && offTime < pause * 1.2)
    return PAUSE;
  if (offTime > letterPause * .8 && offTime < letterPause * 1.2)
    return LETTER_PAUSE;

  unsigned long startTime = millis();
  unsigned long delta = 0;
  while (isPhotoLight()) {
    delayMicroseconds(100);
    delta = millis() - startTime;
    if (delta > 1000) {
      return UNKNOWN;
    }
  }
  // unsigned long delta = millis() - startTime;
  Serial.print("D:");
  Serial.println(delta);
  if (delta > dot * .8 && delta < dot * 1.2)
    return DOT;
  if (delta > dash * .8 && delta < dash * 1.2)
    return DASH;
  if (delta < 10) {
    return PAUSE;
  }

  return UNKNOWN;
}

char decodeToken(Token *tokens, byte len) {
  byte value = 0;
  for (byte j = 0; j < len; j++) {
    value = (value << 1) + (tokens[j] == DOT) ? 0 : 1;
  }
  // Serial.print("l:");
  // Serial.print(len);
  // Serial.print("; v:");
  // Serial.println(value);
  for (byte i = 0; i < SYMBOLS_LEN; i++) {
    Symbol symbol = SYMBOLS[i];
    if (symbol.len == len && symbol.value == value)
      return symbol.symbol;
  }
  return 'n';
}

void setup() {
  Serial.begin(9600);
  // while (!Serial);
  Serial.println("Waiting for incoming data...");
}

Token tokens[10]{};
byte tokenPos = 0;

void loop() {
  Token currentToken = getNextToken();
  Serial.print("T:");
  Serial.println(currentToken);
  char letter = 0;
  switch(currentToken) {
    case PAUSE: return;
    case UNKNOWN: tokenPos = 0; return;
    case LETTER_PAUSE:
      letter = decodeToken(tokens, tokenPos);
      Serial.print(letter);
      tokenPos = 0;
      return;
    case DOT:
    case DASH:
      tokens[tokenPos++] = currentToken;
  }
}