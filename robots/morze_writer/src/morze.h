#include <inttypes.h>

struct Symbol {
  const uint8_t value;
  const char symbol;
  const uint8_t len;
  Symbol(uint8_t v, char s, uint8_t l) : value(v), symbol(s), len(l){}
};

const Symbol SYMBOLS[]{
  Symbol(0b01, 'A', 2),
  Symbol(0b1000, 'B', 4),
  Symbol(0b0101, 'C', 4),
  Symbol(0b100, 'D', 3),
  Symbol(0b0, 'E', 1),
  Symbol(0b0010, 'F', 4),
  Symbol(0b110, 'G', 3),
  Symbol(0b0000, 'H', 4),
  Symbol(0b00, 'I', 2),
  Symbol(0b0111, 'J', 4),
  Symbol(0b101, 'K', 3),
  Symbol(0b0100, 'L', 4),
  Symbol(0b11, 'M', 2),
  Symbol(0b10, 'N', 2),
  Symbol(0b111, 'O', 3),
  Symbol(0b0110, 'P', 4),
  Symbol(0b1101, 'Q', 4),
  Symbol(0b010, 'R', 3),
  Symbol(0b000, 'S', 3),
  Symbol(0b1, 'T', 1),
  Symbol(0b001, 'U', 3),
  Symbol(0b0001, 'V', 4),
  Symbol(0b011, 'W', 3),
  Symbol(0b1001, 'X', 4),
  Symbol(0b1011, 'Y', 4),
  Symbol(0b1100, 'Z', 4),
  Symbol(0b11111, '0', 5),
  Symbol(0b01111, '1', 5),
  Symbol(0b00111, '2', 5),
  Symbol(0b00011, '3', 5),
  Symbol(0b00001, '4', 5),
  Symbol(0b00000, '5', 5),
  Symbol(0b10000, '6', 5),
  Symbol(0b11000, '7', 5),
  Symbol(0b11100, '8', 5),
  Symbol(0b11110, '9', 5),
  Symbol(0b010101, '.', 6),
  Symbol(0b110011, ',', 6),
  Symbol(0b001100, '?', 6),
  Symbol(0b00110, '!', 5),
  Symbol(0b111000, ':', 6),
  Symbol(0b010010, '"', 6),
  Symbol(0b011110, '\'', 6),
  Symbol(0b10001, '=', 5)
};

const uint8_t SYMBOLS_LEN = 43;

const unsigned short timeQuant = 500;

const unsigned short pause = timeQuant / 2;
const unsigned short letterPause = timeQuant;
const unsigned short dot = timeQuant * 2;
const unsigned short dash = timeQuant * 3;