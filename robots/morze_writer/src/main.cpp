#include <Arduino.h>
#include "morze.h"

typedef byte uint8_t;

const int LED_PIN = 13;

void setup() {
  Serial.begin(9600);
  pinMode(LED_PIN, OUTPUT);
}

void turnOn(int milliseconds) {
  digitalWrite(LED_PIN, HIGH);
  delay(milliseconds);
}

void turnOff(int milliseconds) {
  digitalWrite(LED_PIN, LOW);
  delay(milliseconds);
}

void loop() {
  if (Serial.available()) {
    int ch = Serial.read();
    for (int i = 0; i < SYMBOLS_LEN; i++) {
      Symbol s = SYMBOLS[i];
      if (s.symbol == ch) {
        byte value = s.value;
        for (int i = s.len; i > 0; i--) {
          turnOn((value & (1 << (i - 1))) ? dash : dot);
          turnOff(pause);
          // value >>= 1;
        }
        delay(letterPause);
        break;
      }
    }
  }
}