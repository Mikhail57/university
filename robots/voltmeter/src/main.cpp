#include <Arduino.h>

typedef uint8_t byte;

const byte pin = A0;
const int r0 = 9890;
const long resistances[]{216, 9740, 99000};
const byte currentResitorPosition = 1;

void setup() {
  pinMode(pin, INPUT);
  Serial.begin(9600);
}

void loop() {
  float value = analogRead(pin);
  float v0 = value * 5 / 1023;
  const long currentResitance = resistances[currentResitorPosition];
  float voltage = v0 / currentResitance * (r0 + currentResitance);
  Serial.print("V: ");
  Serial.println(voltage);
  delay(250);
}