//
// Created by mikhail on 11/19/18.
//

#include "Field.h"

Field::Field(bool isRedFirst) {
    clear(isRedFirst);
}

void Field::clear(bool isRedFirst) {
    isRedTurn = isRedFirst;
    winner = EMPTY;
    for (int i = 0; i < FIELD_WIDTH; i++) {
        for (int j = 0; j < FIELD_HEIGHT; j++) {
            cells[i][j] = EMPTY;
        }
    }
}

bool Field::makeTurn(int column) {
    if (winner != EMPTY ||
        column < 1 || column > FIELD_WIDTH)
        return false;
    int i = column - 1;
    for (int j = 0; j < FIELD_HEIGHT; j++)
        if (cells[i][j] == EMPTY) {
            cells[i][j] = isRedTurn ? RED : YELLOW;
            checkWinner(); // Победа?
            isRedTurn = !isRedTurn;
            return true;
        }
    return false;
}
