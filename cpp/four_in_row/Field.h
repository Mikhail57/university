//
// Created by mikhail on 11/19/18.
//

#ifndef FOUR_IN_ROW_FIELD_H
#define FOUR_IN_ROW_FIELD_H

#include "Cell.h"

#define FIELD_WIDTH 10
#define FIELD_HEIGHT 10

class Field {
    // Двумерный массив для
    // хранения игрового поля
    Cell cells[FIELD_WIDTH][FIELD_HEIGHT];
    // Очередь хода
    bool isRedTurn;
    // Кто на данный момент выиграл
    Cell winner;
public:
    explicit Field(bool isRedFirst);
    void clear(bool isRedFirst);
    bool makeTurn(int column);
    bool isWon(bool red) const;
    bool isOver() const;
    Cell getCell(int i, int j) const;
    bool isRedTurnNow() const;
    void print() const;
    void printResult() const;
};


#endif //FOUR_IN_ROW_FIELD_H
