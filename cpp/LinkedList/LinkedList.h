//
// Created by mikhail on 11/14/18.
//

#ifndef LINKEDLIST_LINKEDLIST_H
#define LINKEDLIST_LINKEDLIST_H

#include "LinkedListItem.h"
#include "LinkedListException.h"
#include <ostream>

template<class T>
class LinkedList {
    LinkedListItem<T> *start;

    LinkedList(const LinkedList &list);

    LinkedList &operator=(const LinkedList &list);

public:
    LinkedList();
    ~LinkedList();

    LinkedListItem<T> *getStart();
    void deleteFirst();
    void deleteAfter(LinkedListItem<T> *ptr);
    void insertFirst(const T &data);
    void insertAfter(LinkedListItem<T> *ptr, const T &data);

    const LinkedListItem<T> *operator[](int position);

    friend std::ostream &operator<<(std::ostream &out, LinkedList<T> &list);
};

template<class T>
void LinkedList<T>::deleteFirst() {
    if (start) {
        LinkedListItem<T> *second = start->next;
        delete start;
        start = second;
    } else
        throw LinkedListException();
}

template<class T>
LinkedList<T>::LinkedList() {
    start = nullptr;
}

template<class T>
LinkedList<T>::~LinkedList() {
    while (start) {
        deleteFirst();
    }
}

template<class T>
void LinkedList<T>::insertFirst(const T &data) {
    LinkedListItem<T> *second = start;
    start = new LinkedListItem<T>(data, second);
}

template<class T>
LinkedListItem<T> *LinkedList<T>::getStart() {
    return start;
}

template<class T>
void LinkedList<T>::deleteAfter(LinkedListItem<T> *ptr) {
    if (ptr) {
        LinkedListItem<T> *next = ptr->next;
        ptr->next = next->next;
        delete next;
    } else
        throw LinkedListException();
}

template<class T>
void LinkedList<T>::insertAfter(LinkedListItem<T> *ptr, const T &data) {
    if (ptr) {
        LinkedListItem<T> *tmp = ptr->getNext();
        ptr->next = new LinkedListItem<T>(data, tmp);
    } else
        throw LinkedListException();
}

template<class T>
std::ostream &operator<<(std::ostream &out, LinkedList<T> &list) {
    if (list.getStart() != nullptr)
        for (LinkedListItem<T> *ptr = list.start; ptr != nullptr; ptr = ptr->next) {
            out << ptr->data << " ";
        }
    else
        out << "EMPTY";
    return out;
}

template<class T>
const LinkedListItem<T> *LinkedList<T>::operator[](int position) {
    if (position < 0) {
        throw std::invalid_argument("Negative position");
    }
    int listPos = 0;
    LinkedListItem<T> *ptr = start;
    while (listPos < position && ptr) {
        ptr = ptr->getNext();
        listPos++;
    }
    if (position != listPos) {
        throw std::out_of_range("Position exceed list size");
    }
    return ptr;
}


#endif //LINKEDLIST_LINKEDLIST_H
