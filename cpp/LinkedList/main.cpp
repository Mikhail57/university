#include <iostream>
#include "LinkedList.h"

int main() {
    LinkedList<int> list;
    list.insertFirst(15);
    list.insertAfter(list.getStart(), 25);
    std::cout << list << std::endl;
    std::cout << list[1]->getData();
    return 0;
}