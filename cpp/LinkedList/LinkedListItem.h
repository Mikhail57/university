//
// Created by mikhail on 11/14/18.
//

#ifndef LINKEDLIST_LINKEDLISTITEM_H
#define LINKEDLIST_LINKEDLISTITEM_H

template<class T>
class LinkedListItem {
    T data;
    LinkedListItem<T> *next;
    template <typename>
    friend class LinkedList;
public:
    LinkedListItem(const T &data, LinkedListItem<T> *next);
//    ~LinkedListItem();

    const T &getData() const;
    LinkedListItem<T> *getNext();

};

template<class T>
LinkedListItem<T>::LinkedListItem(const T &data, LinkedListItem *next) {
    this->data = data;
    this->next = next;
}

//template<class T>
//LinkedListItem<T>::~LinkedListItem() {
//    delete this->data;
//}

template<class T>
const T &LinkedListItem<T>::getData() const {
    return this->data;
}

//template<class T>
//void LinkedListItem<T>::setData(T newData) {
//    delete this->data;
//    this->data = data;
//}

template<class T>
LinkedListItem<T> *LinkedListItem<T>::getNext() {
    return this->next;
}


#endif //LINKEDLIST_LINKEDLISTITEM_H
