#include <iostream>
#include "Array.h"

int main() {
    Array array(10);

    for (int i = 0; i < 21; i++) {
        array.insert(i);
    }

    array.insert(2, 40);
    array.remove(11);

    for (int i = 0, end = array.getSize(); i < end; i++) {
        std::cout << "Array[" << i << "]=" << array[i] << std::endl;
    }
    std::cout << "Size=" << array.getSize() << std::endl;
    std::cout << "Capacity=" << array.getCapacity() << std::endl;

    return 0;
}