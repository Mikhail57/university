//
// Created by mikhail on 11/2/18.
//

#include "Array.h"

Array::Array(int initialCapacity) {
    this->size = 0;
    this->capacity = initialCapacity;
    this->ptr = new int[capacity];
}

Array::Array(const Array &source) {
    this->capacity = source.capacity;
    this->size = source.size;
    this->ptr = new int[this->capacity];

    for (int i = 0; i < this->size; i++) {
        ptr[i] = source.ptr[i];
    }
}

Array &Array::operator=(const Array &other) {
    if (this == &other) {
        return *this;
    }
    if (capacity != other.capacity) {
        delete[] ptr;
        ptr = new int[other.capacity];
        capacity = other.capacity;
    }
    size = other.size;
    for (int i = 0; i < size; i++) {
        ptr[i] = other.ptr[i];
    }
    return *this;
}

int& Array::operator[](int index) {
    if (index >= size) {
        throw std::invalid_argument("Position is too big");
    } else if (index < 0) {
        throw std::invalid_argument("Negative index");
    }
    return ptr[index];
}

void Array::insert(int value) {
    size++;
    if (size > capacity) {
        resize(capacity * 2);
    }
    ptr[size - 1] = value;
}

void Array::insert(int index, int value) {
    if (index < 0) {
        throw std::invalid_argument("Negative index");
    } else if (index > size) {
        throw std::invalid_argument("Too big index");
    }
    size++;
    if (size > capacity) {
        resize(capacity * 2);
    }
    for (int i = size; i > index; i--) {
        ptr[i] = ptr[i - 1];
    }
    ptr[index] = value;
}

void Array::remove(int index) {
    if (index < 0)
        throw std::invalid_argument("Negative index");
    else if (index >= size)
        throw std::invalid_argument("Index >= size");

    for (int i = index + 1; i < size; i++) {
        ptr[i - 1] = ptr[i];
    }

    if (size < capacity / 2) {
        resize(capacity / 2);
    }
    size--;
}

void Array::resize(int newCapacity) {
    int *newArr = new int[newCapacity];
    int cp = std::min(capacity, newCapacity);
    for (int i = 0; i < cp; i++) {
        newArr[i] = ptr[i];
    }
    capacity = newCapacity;
    delete[] ptr;
    ptr = newArr;
}

int Array::getSize() {
    return size;
}

int Array::getCapacity() {
    return capacity;
}

Array::~Array() {
    delete[] ptr;
}
