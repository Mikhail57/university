//
// Created by mikhail on 11/2/18.
//

#ifndef ARRAY_ARRAY_H
#define ARRAY_ARRAY_H

#include <stdexcept>

const int DEFAULT_CAPACITY = 20;
class Array {
private:
    int *ptr;
    int size, capacity;
    void resize(int);

public:
    explicit Array(int initialCapacity = DEFAULT_CAPACITY);
    explicit Array(const Array&);

    int& operator[](int);
    Array& operator=(const Array&);

    void insert(int);
    void insert(int, int);
    void remove(int);

    int getSize();
    int getCapacity();

    ~Array();
};


#endif //ARRAY_ARRAY_H
