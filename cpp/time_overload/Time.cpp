//
// Created by mikhail on 10/8/18.
//

#include "Time.h"

Time::Time(short reminedHours, short hour, short minute, short seconds) : reminedHours(reminedHours), hour(hour),
                                                                          minute(minute), seconds(seconds) {}

Time::Time(short hour, short minute, short seconds) : hour(hour), minute(minute),
                                                                                 seconds(seconds) {}

Time::Time(short hour, short minute) : hour(hour), minute(minute), seconds(0) {}

Time::Time() : hour(0), minute(0), seconds(0) {}

Time::Time(std::string time) {
    unsigned short t[4]{0, 0, 0, 0};
    size_t p = 0;
    for (char ch : time) {
        if (ch >= '0' && ch <= '9') {
            t[p] = t[p] * 10 + (ch - '0');
        } else if (ch == ':') {
            p++;
        } else {
            throw std::invalid_argument("Invalid char");
        }
        if (p > 3) {
            throw std::invalid_argument("Too many semicolons");
        }
    }
    if (p == 0)
        throw std::invalid_argument("Invalid time");
    hour = t[0];
    minute = t[1];
    seconds = t[2];
}

Time Time::operator+(const Time &other) {
    short tSeconds = this->seconds + other.seconds;
    short tMinutes = this->minute + other.minute + tSeconds / 60;
    short tHour = this->hour + other.hour + tMinutes / 60;

    short reminder = 0;

    if (tHour > 23) {
        reminder = tHour - 24;
        tHour %= 24;
    }
    return Time(reminder, tHour, tMinutes % 60, tSeconds % 60);
}

Time Time::operator-(const Time &other) {
    short tSeconds = this->seconds - other.seconds;
    short tMinutes = this->minute - other.minute + (tSeconds < 0 ? -1 : 0);
    short tHour = this->hour - other.hour + (tMinutes < 0 ? -1 : 0);

    if (tMinutes < 0) {
        tMinutes += 60;
    }
    if (tSeconds < 0) {
        tSeconds += 60;
    }

    short reminder = 0;

    if (tHour < 0) {
        reminder = tHour;
        tHour += 24;
    }

    return Time(reminder, tHour, tMinutes, tSeconds);
}

Time Time::operator+(double hours) {
    short h = hours;
    double afterDot = hours - h;
    short m = 60 * afterDot;
    Time other(h,m);
    return operator+(other);
}

Time Time::operator-(double hours) {
    return operator+(-hours);
}

std::ostream &operator<<(std::ostream &output, const Time &t) {
    output << std::setfill('0') << std::setw(2) << t.hour;
    output << ":";
    output << std::setfill('0') << std::setw(2) << t.minute;
    output << ":";
    output << std::setfill('0') << std::setw(2) << t.seconds;
    return output;
}
