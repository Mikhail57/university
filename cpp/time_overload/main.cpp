#include <iostream>
#include "Time.h"

using namespace std;

int main() {
    Time t1("12:15");
    Time t2(10, 50);
    Time t6("7:00");
    Time t7("10:00");

    Time t3 = t1 + t2;
    Time t4 = t3 - t2;
    Time t5 = t1 + 10.5;
    Time t8 = t6-t7;
    Time t9 = t7-11;

    cout << t1 << endl;
    cout << t2 << endl;
    cout << t3 << endl;
    cout << t4 << endl;
    cout << t5 << endl;
    cout << t8 << endl;
    cout << t9 << endl;
    return 0;
}