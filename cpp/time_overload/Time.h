//
// Created by mikhail on 10/8/18.
//

#ifndef TIME_OVERLOAD_TIME_H
#define TIME_OVERLOAD_TIME_H

#include <iostream>
#include <string>
#include <stdexcept>
#include <iomanip>

class Time {
protected:
    short reminedHours = 0;
    Time(short reminedHours, short hour, short minute, short seconds);

public:
    short hour;
    short minute;
    short seconds;

    Time(short hour, short minute, short seconds);

    Time(short hour, short minute);

    Time(std::string time);

    Time();

    Time operator+(const Time &other);

    Time operator-(const Time &other);

    Time operator+(double hours);
    Time operator-(double hours);

    friend std::ostream &operator<<(std::ostream &output, const Time &t);
};


#endif //TIME_OVERLOAD_TIME_H
