#include <iostream>
#include <vector>
#include <stack>
#include <cmath>

using namespace std;

const double INF = INFINITY;

const unsigned long n = 6;
vector<vector<pair<int, double>>> matrix;

void setup() {
    matrix.resize(n);

    matrix[0].resize(3);
    matrix[0][0].first = 1;
    matrix[0][0].second = 3;
    matrix[0][1].first = 2;
    matrix[0][1].second = 2;
    matrix[0][2].first = 3;
    matrix[0][2].second = 4;

    matrix[1].resize(2);
    matrix[1][0].first = 4;
    matrix[1][0].second = 2;
    matrix[1][1].first = 5;
    matrix[1][1].second = 4;

    matrix[2].resize(1);
    matrix[2][0].first = 4;
    matrix[2][0].second = 1;

    matrix[3].resize(1);
    matrix[3][0].first = 5;
    matrix[3][0].second = 3.5;

    matrix[4].resize(1);
    matrix[4][0].first = 5;
    matrix[4][0].second = 3;
}

pair<vector<int>, double> calculate(int start, int end) {
    vector<double> distances(n, INF);
    vector<int> path(n);
    distances[start] = 0;
    vector<bool> visited(n);
    for (int i = 0; i < n; i++) {
        int v = -1;
        for (int j = 0; j < n; j++) {
            if (!visited[j] && (v == -1 || distances[j] < distances[v]))
                v = j;
        }
        if (distances[v] == INF)
            throw std::exception();
        visited[v] = true;
        for (int j = 0; j < matrix[v].size(); j++) {
            int to = matrix[v][j].first;
            double dst = matrix[v][j].second;
            if (distances[v] + dst < distances[to]) {
                distances[to] = distances[v] + dst;
                path[to] = v;
            }
        }
    }
    int currentPos = end;
    stack<int> outPathStack;
    while (currentPos != start) {
        outPathStack.push(currentPos);
        currentPos = path[currentPos];
    }
    outPathStack.push(start);
    vector<int> outPath;
    double distance = 0;
    while (!outPathStack.empty()) {
        outPath.emplace_back(outPathStack.top());
        outPathStack.pop();
    }


    for (int i = 0; i < outPath.size() - 1; i++) {
//        cout << i << " ";
        double tmpDst = 0;
        for (auto p : matrix[i]) {
            if (p.first == outPath[i + 1]) {
                tmpDst = p.second;
                break;
            }
        }
        distance += tmpDst;
    }

    for (auto point : outPath) {
        cout << point << " ";
    }
    cout << endl;
    cout << "Dst: " << distance;

//    return pair(outPath, distance);
}

int main() {
    setup();
    calculate(0, 5);
    return 0;
}