//
// Created by mikhail on 10/8/18.
//

#ifndef RATIONAL_RATIONAL_H
#define RATIONAL_RATIONAL_H

#include <iostream>

class Rational {
private:
    int nominator;
    int denominator;

    void simplify();

public:
    Rational();
    Rational(int number);
    Rational(int nominator, int denominator);

    Rational &operator+=(const Rational &other);
    Rational operator+(const Rational &other) const;
    Rational &operator-=(const Rational &other);
    Rational operator-() const;
    Rational operator-(const Rational& other) const;
    Rational &operator*=(const Rational &other);
    Rational operator*(const Rational &other) const;
    Rational &operator/=(const Rational &other);
    Rational operator/(const Rational &other) const;

    Rational &operator++();
    const Rational operator++(int);

    bool operator==(Rational &other) const;
    bool operator!=(Rational &other) const;
    bool operator>(Rational &other) const;
    bool operator>=(Rational &other) const;
    bool operator<(Rational &other) const;
    bool operator<=(Rational &other) const;

    explicit operator int();
    explicit operator double();

    friend std::istream &operator>>(std::istream &in, Rational &rational);
    friend std::ostream &operator<<(std::ostream &out, const Rational &rational);

//    friend
};


#endif //RATIONAL_RATIONAL_H
