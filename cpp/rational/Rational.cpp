//
// Created by mikhail on 10/8/18.
//

#include "Rational.h"
#include <algorithm>

Rational::Rational() {
    nominator = 0;
    denominator = 1;
}

Rational::Rational(int number) {
    nominator = number;
    denominator = 1;
}

Rational::Rational(int nominator, int denominator) {
    this->nominator = nominator;
    this->denominator = denominator;
}

Rational &Rational::operator+=(const Rational &other) {
    nominator = nominator * other.denominator + other.nominator * denominator;
    denominator *= other.denominator;
    simplify();
    return *this;
}

void Rational::simplify() {
    int gcd = std::__algo_gcd(abs(nominator), abs(denominator));
    nominator /= gcd;
    denominator /= gcd;
}

Rational Rational::operator+(const Rational &other) const {
    Rational res(*this);
    return res += other;
}

Rational Rational::operator-() const {
    return {-nominator, denominator};
}

Rational Rational::operator-(const Rational &other) const {
    return *this + -other;
}

Rational &Rational::operator-=(const Rational &other) {
    return (*this += (-other));
}

Rational &Rational::operator*=(const Rational &other) {
    nominator *= other.nominator;
    denominator *= other.denominator;
    simplify();
    return *this;
}

Rational Rational::operator*(const Rational &other) const {
    return Rational(*this) *= other;
}

Rational &Rational::operator/=(const Rational &other) {
    nominator *= other.denominator;
    denominator *= other.nominator;
    simplify();
    return *this;
}

Rational Rational::operator/(const Rational &other) const {
    return Rational(*this) /= other;
}

Rational &Rational::operator++() {
    nominator += denominator;
    return *this;
}

const Rational Rational::operator++(int) {
    Rational orig(*this);
    nominator += denominator;
    return orig;
}

bool Rational::operator==(Rational &other) const {
    return nominator == other.nominator && denominator == other.denominator;
}

bool Rational::operator!=(Rational &other) const {
    return !(*this == other);
}

bool Rational::operator>(Rational &other) const {
    int nom1 = nominator * other.denominator;
    int nom2 = other.nominator * denominator;
    return nom1 > nom2;
}

bool Rational::operator<(Rational &other) const {
    int nom1 = nominator * other.denominator;
    int nom2 = other.nominator * denominator;
    return nom1 < nom2;
}

bool Rational::operator>=(Rational &other) const {
    int nom1 = nominator * other.denominator;
    int nom2 = other.nominator * denominator;
    return nom1 >= nom2;
}

bool Rational::operator<=(Rational &other) const {
    int nom1 = nominator * other.denominator;
    int nom2 = other.nominator * denominator;
    return nom1 <= nom2;
}

Rational::operator int() {
    return nominator / denominator;
}

Rational::operator double() {
    return (double) nominator / denominator;
}

std::istream &operator>>(std::istream &in, Rational &rational) {
    in >> rational.nominator >> rational.denominator;
    return in;
}

std::ostream &operator<<(std::ostream &out, const Rational &rational) {
    out << rational.nominator << "/" << rational.denominator;
    return out;
}


