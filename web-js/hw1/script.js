(function() {

  function handleSubmitButtonClick() {
    var firstName = document.getElementById('first-name');
    var middleName = document.getElementById('middle-name');
    var noMiddle = document.getElementById('have-no-middle-name');
    var lastName = document.getElementById('last-name');

    var fieldErrorClass = 'error';

    if (firstName.value.length < 1) {
      firstName.classList.add(fieldErrorClass);
    } else {
      firstName.classList.remove(fieldErrorClass);
    }
    if (lastName.value.length < 1) {
      lastName.classList.add(fieldErrorClass);
    } else {
      lastName.classList.remove(fieldErrorClass);
    }
    if (!noMiddle.checked && middleName.value.length < 1) {
      middleName.classList.add(fieldErrorClass);
    } else {
      middleName.classList.remove(fieldErrorClass);
    }
  }

  var submitButton = document.getElementById('submit');
  submitButton.addEventListener('click', function(ev) {
    ev.preventDefault();
    handleSubmitButtonClick();
  });

  function handleNoMiddleNameChange(value) {
    var middleName = document.getElementById('middle-name').parentElement;
    middleName.style.display = value ? 'none' : '';
  }

  var noMiddleToggle = document.getElementById('have-no-middle-name');
  noMiddleToggle.addEventListener('change', function() {
    handleNoMiddleNameChange(this.checked);
  });

})();