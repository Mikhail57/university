(function() {
  var CONTENT_ID = 'content';
  var TEXT_COLOR_ID = 'text-color';
  var BACK_COLOR_ID = 'background-color';
  var FONT_STYLE_ID = 'font-style';
  var RESULT_ID = 'result';

  var ERROR_FIELD_CLASS = 'error-field';

  var textColorInput = document.getElementById(TEXT_COLOR_ID);
  var backColorInput = document.getElementById(BACK_COLOR_ID);

  var resultElement = document.getElementById(RESULT_ID);

  document.getElementById(CONTENT_ID).addEventListener('input', function() {
    resultElement.innerText = this.value;
  });

  textColorInput.addEventListener('input', function() {
    handleTextColorChanges(this);
  });

  backColorInput.addEventListener('input', function() {
    handleBackColorChanges(this);
  });

  document.getElementById(FONT_STYLE_ID).addEventListener('change', function() {
    resultElement.style.fontStyle = this.options[this.selectedIndex].value;
  });

  var fwElements = document.getElementsByName('font-weight');
  for (var i = 0; i < fwElements.length; i++) {
    var element = fwElements[i];
    element.addEventListener('change', function() {
      if (this.checked) {
        resultElement.style.fontWeight = this.value;
      }
    });
  }

  function handleTextColorChanges(element) {
    if (isValidColor(element.value)) {
      resultElement.style.color = element.value;
      element.classList.remove(ERROR_FIELD_CLASS);
    } else {
      element.classList.add(ERROR_FIELD_CLASS);
    }
  }

  function handleBackColorChanges(element) {
    resultElement.style.backgroundColor = element.value;
  }

  function isValidColor(color) {
    if (color) {
      var matches = color.match(/^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/g);
      return matches && matches.length === 1;
    }
    return false;
  }

  function generateRandomHexColor() {
    var color = '#';
    var letters = '0123456789ABCDEF';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  document.getElementById('random-color').addEventListener('click', function() {
    backColorInput.value = generateRandomHexColor();
    textColorInput.value = generateRandomHexColor();
    handleTextColorChanges(textColorInput);
    handleBackColorChanges(backColorInput);
  });
})();