function gcd(a, b) {
  a = Math.abs(a);
  b = Math.abs(b);
  if (b > a) {
    var temp = a;
    a = b;
    b = temp;
  }
  while (true) {
    if (b === 0) return a;
    a %= b;
    if (a === 0) return b;
    b %= a;
  }
}

function isPrime(a) {
  for (var i = 2, end = Math.sqrt(a) + 1; i < end; i++) {
    if (a % i === 0) {
      return false;
    }
  }
  return true;
}

function sieveOfEratosthenes(n) {
  if (n < 1) {
    return null;
  }
  var arr = [];
  for (var i = 0; i <= n; i++) {
    arr.push(true);
  }
  for (var i = 2, end = Math.sqrt(n); i < end; i++) {
    if (arr[i]) {
      for (var j = i * i; j <= n; j += i) {
        arr[j] = false;
      }
    }
  }
  var result = [];
  for (var i = 2; i <= n; i++) {
    if (arr[i]) {
      result.push(i);
    }
  }
  return result;
}

function dividersOf(x) {
  result = [];
  for (var i = 1, end = Math.sqrt(x) + 1; i < end; i++) {
    if (x % i === 0) {
      result.push(i);
    }
  }
  return result;
}

function getUniqueFactorization(x) {
  var result = [];
  var n = x;
  var div = 2;
  while (n > 1 && div <= n) {
    if (n % div === 0) {
      n /= div;
      result.push(div);
    } else {
      div++;
    }
  }
  return result;
}

function pract1_1(a, b) {
  return gcd(a, b) === 1;
}

function pract1_2(a) {
  return isPrime(a);
}

function pract1_3(n) {
  return sieveOfEratosthenes(n);
}

function pract1_5(n) {
  return dividersOf(n);
}

function pract1_6(x) {
  var uf = getUniqueFactorization(x);
  var res = '';
  var count = 1;
  var prev = uf[0];
  var length = uf.length;
  for (var i = 1; i < length; i++) {
    var value = uf[i];
    if (value !== prev) {
      res += prev + '^' + count + '+';
      prev = value;
      count = 1;
    } else {
      count++;
    }
  }
  res += value + '^' + count;
  return res;
}

function getAllDividers(x) {
  var result = [];
  for (var i = 1, end = x / 2; i <= end; i++) {
    if (x % i === 0)
      result.push(i);
  }
  return result;
}

function sum(a, b) {
  return a + b;
}

function isPerfectNumber(x) {
  return x === getAllDividers(x).reduce(sum);
}

function getPerfectNumber(n) {
  var k = 0;
  var p = 2;
  var x = 0;
  var pow = Math.pow;
  while (k < n) {
    x = pow(2, p - 1) * (pow(2, p) - 1);
    if (isPerfectNumber(x)) {
      k++;
    }
    p++;
  }
  return x;
}

function pract1_7(n) {
  return getPerfectNumber(n);
}

function pract1_8(n, m) {
  var results = [];
  outer:
    for (var i = n; i <= m; i++) {
      var primes = sieveOfEratosthenes(i);
      for (var v1 = 0, len = primes.length; v1 < len; v1++) {
        for (var v2 = v1 + 1; v2 < len; v2++) {
          for (var v3 = v2 + 1; v3 < len; v3++) {
            if (v1 + v2 + v3 === i) {
              results.push(i + '=' + v1 + '+' + v2 + '+' + v3);
              break outer;
            }
          }
        }
      }
    }
  return results;
}

function isSquare(n) {
  var a = Math.floor(Math.sqrt(n));
  return Math.abs((a * a) - n) < 1e-10;
}

function isFib(n) {
  return isSquare(5 * n * n - 4) || isSquare(5 * n * n + 4);
}

function pract1_9(n) {
  return isFib(n);
}

function pract1_10(n) {
  var a = n;
  var result = 1;
  while (a > 0) {
    result *= a;
    a -= 2;
  }
  return result;
}

var result = document.getElementById('result');

var input = document.getElementById('number');
var input2 = document.getElementById('number2');

var btn1_1 = document.getElementById('1-1');
var btn1_2 = document.getElementById('1-2');
var btn1_3 = document.getElementById('1-3');
var btn1_4 = document.getElementById('1-4');
var btn1_5 = document.getElementById('1-5');
var btn1_6 = document.getElementById('1-6');
var btn1_7 = document.getElementById('1-7');
var btn1_8 = document.getElementById('1-8');
var btn1_9 = document.getElementById('1-9');
var btn1_10 = document.getElementById('1-10');
// var btn1_11 = document.getElementById('1-11');
// var btn1_12 = document.getElementById('1-12');
// var btn1_13 = document.getElementById('1-13');
// var btn1_14 = document.getElementById('1-14');
// var btn1_15 = document.getElementById('1-15');
//
btn1_1.addEventListener('click', function(ev) {
  result.innerText = pract1_1(+input.value, +input2.value);
});
btn1_2.addEventListener('click', function(ev) {
  result.innerText = pract1_2(+input.value) + '';
});
btn1_3.addEventListener('click', function(ev) {
  result.innerText = pract1_3(+input.value) + '';
});
// btn1_4.addEventListener('click', function(ev) {
//   result.innerText = pract1_4(+input.value) + '';
// });
btn1_5.addEventListener('click', function(ev) {
  result.innerText = pract1_5(+input.value) + '';
});
btn1_6.addEventListener('click', function() {
  result.innerText = pract1_6(+input.value);
});
btn1_7.addEventListener('click', function(ev) {
  result.innerText = pract1_7(+input.value);
});
btn1_8.addEventListener('click', function(ev) {
  result.innerText = pract1_8(+input.value, +input2.value) + '';
});
btn1_9.addEventListener('click', function(ev) {
  result.innerText = pract1_9(+input.value) + '';
});
btn1_10.addEventListener('click', function(ev) {
  result.innerText = pract1_10(+input.value) + '';
});

// pract1_9(1);
