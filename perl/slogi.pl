#!/usr/bin/perl

use strict;
use utf8;

binmode STDIN, ":utf8";
binmode STDOUT, ":utf8";
binmode STDERR, ":utf8";

my $glasniye = qr/[ауеоэяиюы]/;
my $sonarniye = qr/[лмнр]/;

my @mass=qw(я вышел погулять и увидел как через реку строят новый мост);
for my $word (@mass){
	my $last = 0;
	my $current = 0;
	my $result_word = "";
	for my $letter (split //, $word) {
		if ($letter =~ $glasniye) {
			$current=3;
		} elsif ($letter =~ $sonarniye) {
			$current=2;
		} elsif ($letter =~ /й/) {
			$current=4;
		} else {
			$current=1;
		}
		# print $letter,'(',$current,')';
		if ($last gt $current or ($last eq 3 and $current eq 3)) {
			$result_word .= '-' . $letter;
		} else {
			$result_word .= $letter;
		}
		$last=$current;
		# print $letter,"-";
	}
	my $count_dash = () = $result_word =~ /\-/g;
	my $count_glasniye = () = $result_word =~ /$glasniye/g;
	# print $count_dash,'-',$count_glasniye,' ';

	if ($count_dash == $count_glasniye) {
		$result_word =~ s/\-(\w*)$/$1/;
		# print '+++';
	}
	print $result_word,"\n";
	# print $word,"\n";
}